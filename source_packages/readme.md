GAME: Anamnesis 
(Nome de projeto SUS Generis)

SOURCE PACKAGES

Ferramentas Utilizada para Desenvolvimento:

Godot Game Engine
Versão Utilizada: 3.1.1 Final
https://godotengine.org/
https://downloads.tuxfamily.org/godotengine/3.1.1/

Yarn Json Visual Editor 
(Não é mandatória, mas auxilia enormemente a edição dos Jsons usados pelo jogo)
Versão Utilizada: 0.2.1
https://github.com/YarnSpinnerTool/YarnEditor