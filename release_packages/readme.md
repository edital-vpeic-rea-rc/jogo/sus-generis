GAME: Anamnesis 
(Nome de projeto SUS Generis)

RELEASES

Sistemas Operacionais Necessários:

Build Windows: Versão 7 ou superior
Build Mac: Versão 10.12 (Sierra) ou superior
Build Linux: Debian/Ubuntu recentes (prefencialmente)

OBS: No caso da Versão Mac pode ser necessário autorização do administrador para rodar a primeira vez. Daí em diante, poderá rodar normalmente.